# -*- coding: utf-8 -*-
import os
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow.keras.utils import Progbar
import copy
import pickle
tf.random.set_seed(56)
import pandas as pd
import shutil

from architecture import architecture
from environment import environment
from variables import logger, path_history, input_shape, path_all_history, n_channels
from ctx import ctx


def unison_shuffled_copies(a, b):
    """Shuffles patient's paths and names together"""
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]

def tf_count(t, val):
    """ counts a value in tensor"""
    elements_equal_to_value = tf.equal(t, val)
    as_ints = tf.cast(elements_equal_to_value, tf.int32)
    count = tf.reduce_sum(as_ints)
    return count

class AI_Sampling:

    def __init__(self, 
                 save_loss=False,
                 save_softmax=False
                 ):
        """Manages the training process
        Parameters
        ----------
        input_shape : str
            shape of the input for the model. Defined globally
        n_channels : str
            number of input channels. Defined globally
        optimizer : str
            optimizer
        save_loss : bool
            whether the loss matrix should be saved (default: False). It is used for the debugging of the loss function
        save_softmax : bool
            whether the softmax matrix should be saved (default: False). It is used for the debugging of the loss function
        """
        self.input_shape = input_shape
        self.n_channels=n_channels
        self.model = architecture(input_shape, n_channels)
        self.optimizer = tf.keras.optimizers.Adam(learning_rate = 1e-3)
        self.logger=logger
        logger.debug(f"start with LR {self.optimizer.learning_rate}")
        self.learning_rate= self.optimizer.learning_rate
        self.save_loss=save_loss
        self.save_softmax=save_softmax
        self.create_history_path(path_history=path_history, save_loss=save_loss, save_softmax=save_softmax)
        
    def decrease_lr(self,epoch):
        """ Learning rate scheduler"""
        if epoch//5==1:
            self.learning_rate= self.learning_rate/10
            self.optimizer.learning_rate=self.learning_rate
        elif epoch//5>1 and self.learning_rate/(epoch//5*2)>1e-5:
            self.optimizer.learning_rate=self.learning_rate/(epoch//5*2)
        else:
            logger.debug("the learning rate will not be reduced")
        logger.debug(f"new learning rate is {self.optimizer.learning_rate.numpy()} and start LR is {self.learning_rate.numpy()} devided by {(epoch//5*2)}")
    
    def create_history_path(self, path_history, save_loss, save_softmax):
        if save_loss:
            self.path_history_loss = os.path.join(path_history, "loss")
            os.makedirs(self.path_history_loss, exist_ok=True)
        if save_softmax:
            self.path_history_softmax = os.path.join(path_history, "softmax")
            os.makedirs(self.path_history_softmax, exist_ok=True)
  
    def train(self, path_list,
              patients_list,
              steps_pro_episode=5,
              epochs = 20,
              resume_training = False,
              path_resume = None,
              path_save_checkpoint = os.getcwd(),
              save_epoch_weights=True,
              dose_threshold=0.985,
              planned_dose=3,
              oar_labels="RM", 
              save_csv=False,
              debug=False,
              path_TRiP=""):

        """
        starts full training process including initialisation of the environment
        Parameters
        ----------
        path_list : list
            list of patient's individual paths
        patients_list : list
            list of patient's individual names
        epochs : int
            number of epochs to train
        resume_training : bool
            training should be resumed
        path_resume : str
            path where the training should be resumed from
        path_save_checkpoint : str
            path to save checkpoints. Default is the working directory
        save_epoch_weights : bool
            the weight at the end of each epoch is saved separately
        dose_threshold : float
            dose threshold for the loss function. Value between 0 and 1
        planned_dose : float
            planned dose to calculate the percentage from
        oar_labels : str
            whether heuristic sampling from the patient script (RM) or all voxels (FM) should be used as labels for organs at risk
        save_csv : bool
            saves csv data with dose coverage data provided by TRiP98 at each step.
        debug : bool
            saves dose map, label map and mask for TRiP98 to the history
        path_TRiP : str
            path to TRiP98
        """
        #Set initial variables
        shuffle_patients = True
        training_resumed = False
        epochs_passed = 0
        patient_num_passed = 0
        count_steps = 0

        if resume_training:
               
            if os.path.isfile(os.path.join(path_save_checkpoint, "checkpoint.h5")):
                logger.info(f"Loading weights from {path_save_checkpoint}")
                self.model(np.zeros((1,*reversed(self.input_shape), self.n_channels)))
                self.model.load_weights(os.path.join(path_save_checkpoint, "checkpoint.h5"))
            else:
                logger.info("Cannot find any model to load")
                return
            with open(os.path.join(path_save_checkpoint,'model_epoch.txt'), "r") as f:
                line = f.readline()
                line = line.split(",")
                epochs_passed = int(line[0])
                patient_num_passed = int(line[1])
                count_steps = int(line[2])
                if patient_num_passed==len(path_list)-1 and count_steps==steps_pro_episode-1:
                    epochs_passed+=1
                    patient_num_passed = 0
                    count_steps = 0
                elif count_steps==steps_pro_episode-1:
                    patient_num_passed += 1
                    count_steps = 0
            logger.debug(f"patient list before resuming training\n{patients_list}")
            if patient_num_passed!=0 and count_steps!=0: #restore patients if it is not the beginning of an epoch
                with open(os.path.join(path_save_checkpoint,"path_list"), "rb") as pl:   # Unpickling
                    path_list= pickle.load(pl)
                with open(os.path.join(path_save_checkpoint,"patients_list"), "rb") as pl:   # Unpickling
                    patients_list= pickle.load(pl)
                shuffle_patients=False
                logger.debug(f"patient list after resuming training\n{patients_list}")


        for epoch in range(epochs_passed,epochs):
            self.decrease_lr(epoch)
            
            if shuffle_patients:
                path_list,patients_list = unison_shuffled_copies(np.array(path_list),np.array(patients_list))
                logger.debug(f"patient list after shuffeling\n{patients_list}")
            else:
                #after resuming once the patients should not be shuffled
                shuffle_patients=True
                
            print(f"\nepoch {epoch+1}/{epochs}")
            pb_i = Progbar(len(path_list))
            
            #write new order of patiens
            with open(os.path.join(path_save_checkpoint,"path_list"), "wb") as pl:   #Pickling
                pickle.dump(path_list, pl)
            with open(os.path.join(path_save_checkpoint,"patients_list"), "wb") as pl:   #Pickling
                pickle.dump(patients_list, pl)
                

            for patient_num in range(patient_num_passed,len(path_list)):
                if self.save_loss:    
                    loss_df = pd.DataFrame({'Epoch':[epoch],
                                            'Sample':[patient_num],
                                            'PatientID' : [patients_list[patient_num]]
                                            })
                
                logger.debug(f"I am at {patients_list[patient_num]}.Epoch {epoch}. Initialize environment")
                
                #remove FIFO in  current folder if it is there after prior training
                os.remove(os.path.join(path_list[patient_num], "check")) if os.path.exists(os.path.join(path_list[patient_num], "check")) else None

                env = environment(patient = patients_list[patient_num],
                                  path_to_FIFO= os.path.join(path_list[patient_num], "check"),
                                  path= path_list[patient_num],
                                  script = "refineMask", space ="AfterRefineLabel.ctx", random_mask = "AfterRefine.ctx",
                                  label_map = "Label4AI.ctx", target_label = 2, dose_map = "Dose4AI.ctx",
                                  dist_map = "DistanceTransform.ctx", new_shape=self.input_shape,
                                  labels= oar_labels,
                                  path_history = path_history,
                                  epoch=epoch,
                                  start_TRiP=True,
                                  path_TRiP=path_TRiP,
                                  save_csv=save_csv,
                                  debug=debug
                                  )
                
                if resume_training and not training_resumed:
                    if save_csv:
                        env.resume_history()
                        #resuming the history
                        env.count_steps = count_steps
                        if self.save_loss:
                            loss_df_path = os.path.join(path_history, patients_list[patient_num], f"{patients_list[patient_num]}_history_loss_epoch_{epoch}.csv")
                        if os.path.isfile(loss_df_path):
                            loss_df =  pd.read_csv(loss_df_path) 
                        logger.debug(f"Resumed temp history\n{env.history_DB}")   
                    training_resumed=True

                for iteration in range(count_steps, steps_pro_episode):
                    logger.debug(f"I am at {patients_list[patient_num]}.Epoch {epoch} out of {epochs-1}. Patient {patient_num} out of {len(path_list)-1}.Step num {iteration} out of {steps_pro_episode-1}")
                    
                    with tf.GradientTape() as tape:
                        #get labels for the CE loss in OARs
                        labels = np.expand_dims(env.get_output(), axis=0)
                        labels = np.where(labels==2,1,0)
                        labels = (labels[:,:,:,:,None] == np.unique(labels)).astype(int)
                        logger.debug(f"Final shape of the output map: {labels.shape}")
                        
                        #get full label map to count voxles in the target
                        label_map_loc = np.expand_dims( env.get_labels(), axis=0)
                        label_map_loc = tf.constant(label_map_loc, dtype=tf.float32)
                        target_label_counter = tf_count(label_map_loc, 2)
                        logger.debug(f"target label_counter {target_label_counter.numpy()}")

                        #get input space for the model
                        inp =np.expand_dims( env.get_space(), axis=0)
                        
                        #get softmax walues from the model
                        softmax = self.model(inp, training=True)  # Logits for this minibatch
                        if self.save_softmax:
                            print(f"save_softmax is {self.save_softmax}")
                            np.save(os.path.join(self.path_history_softmax , str("action_softmax_"+str(patients_list[patient_num])+"_epoch_"+str(epoch)+"_"+str(iteration)+".npy")), softmax.numpy())
                        logger.debug(f"Final shape of the softmax: {softmax.shape}")
                        
                        #create distribution of the softmax to sample voxels
                        distr = tfp.distributions.Categorical(probs=softmax, dtype=tf.float32)
                        opt_mask = distr.sample()
                        
                        #clip min max values of softmax not to get inf in cross-entropy loss
                        softmax_clipped = tf.clip_by_value(softmax,1e-20,1)

                        target_mask = tf.where((label_map_loc==2) & (opt_mask==1),label_map_loc,0)
                        action_target_label_counter = tf_count(target_mask, 2)
                        target_mask = tf.cast(target_mask, tf.float32)
                        #get fraction of selected voxels in the target
                        percent = tf.math.divide(action_target_label_counter,target_label_counter)
                        percent = tf.cast(percent, tf.float32)
                        
                                              
                        #calculate cross-entropy for ection
                        #TODO: can we use mask to mask target
                        loss_value_ce = -tf.reduce_sum(labels * tf.math.log(softmax_clipped), -1)#tf.reduce_mean()

                        
                        env.step(opt_mask)
                        #read new dose 
                        dose_map = tf.constant(env.get_dose(), dtype=tf.float32)
                        #set overdosed voxels to the planned dose value
                        dose_map_clipped = tf.clip_by_value(dose_map,0,planned_dose)
                        dose_map_percent = dose_map_clipped/planned_dose

                        
                        true_label_map = tf.where(dose_map_percent <= dose_threshold , 
                                                1,
                                                0)
                        true_label_map = tf.cast(true_label_map, tf.int32)
                        true_label_map_one_hot = tf.one_hot(true_label_map,2, dtype=tf.int32)
                       
                        loss_value_full=tf.where(label_map_loc == 2, 
                                                 -tf.reduce_sum(true_label_map_one_hot * tf.math.log(softmax_clipped), -1),
                                                 
                                                loss_value_ce)
                        loss_value_full=tf.where((label_map_loc == 2) & (true_label_map==0), 
                                                loss_value_full*percent,
                                                 
                                                loss_value_ce)
                        
                        if self.save_loss:    
                            np.save(os.path.join(self.path_history_loss,str("loss_"+str(patients_list[patient_num])+"_epoch_"+str(epoch)+"_"+str(iteration)+".npy")), loss_value_full)

                        loss_target=tf.math.reduce_sum(tf.where(label_map_loc == 2, loss_value_full, tf.cast(0, tf.float32)))/tf.math.count_nonzero(tf.where(label_map_loc == 2, 1, tf.cast(0, tf.float32)), dtype=tf.float32)
                        loss_rest=tf.math.reduce_sum(tf.where(label_map_loc != 2, loss_value_full, tf.cast(0, tf.float32)))/tf.math.count_nonzero(tf.where(label_map_loc !=2, 1, tf.cast(0, tf.float32)), dtype=tf.float32)                        
                        loss_value = loss_rest+loss_target
                        
                        if self.save_loss:
                            loss_df[str("CCE_"+str(iteration))]=float(loss_rest.numpy())  
                            loss_df[str("Target_"+str(iteration))]=float(loss_target.numpy())
                            loss_df[str("Total_"+str(iteration))]=float(tf.reduce_mean(loss_value_full).numpy())
                            logger.debug(f"\n{loss_df}")
                            
                        logger.debug(f"Target percentage: {percent.numpy()}")
                        logger.debug(f"loss value: {loss_target.numpy()}")
                        if save_csv:
                            env.history_DB.loc[env.history_DB["Category"]=="Loss", str("NM_"+ str(iteration))]=float(loss_target.numpy())
                            env.history_DB.loc[:,"Epoch"]=epoch
                            env.history_DB.loc[:,"Sample"]=patient_num
                            env.write_history(path_history,Local = True, local_name = None, Global = False)

                        
                        
                    # Use the gradient tape to automatically retrieve
                    # the gradients of the trainable variables with respect to the loss.
                    grads = tape.gradient(loss_value, self.model.trainable_weights)
                                       
                    
                    # Run one step of gradient descent by updating
                    # the value of the variables to minimize the loss.
                    logger.debug("Appling the gradients")
                    self.optimizer.apply_gradients(zip(grads, self.model.trainable_weights))
                    logger.debug("Write the checkppoint")
                    self.model.save_weights(os.path.join(path_save_checkpoint, "checkpoint.h5"), save_format="h5") 
                    with open(os.path.join(path_save_checkpoint,'model_epoch.txt'), "w") as f:
                        f.write(str(epoch))
                        f.write(",")
                        f.write(str(patient_num))
                        f.write(",")
                        f.write(str(iteration))

                    try: 
                        path_history_weights_model = os.path.join(path_all_history, "weights", str(str(epoch)+"_epoch"))
                        os.makedirs(path_history_weights_model, exist_ok=True)
                        shutil.copytree(path_save_checkpoint, os.path.join(path_history_weights_model, str(patients_list[patient_num])+"_"+str(epoch)+"_"+str(patient_num)+"_"+str(iteration)))
                    except: 
                        logger.debug("Checkpoint could not be saved")
                        pass
                    
                    if self.save_loss:
                        path_local = os.path.join(path_history, patients_list[patient_num])
                        os.makedirs(path_local, exist_ok=True)
                        loss_df.to_csv(os.path.join(path_local, str(patients_list[patient_num]+"_history_loss_epoch_"+str(epoch)+".csv")), index=False)
                    
             
               
                env.stop_env()
                
                if save_csv:
                    env.write_history(path_history,Local = False, local_name = None, Global = True)
               
                if self.save_loss:
                    path_global = os.path.join(path_history, "history_loss_whole.csv")
                    loss_df.to_csv(path_global, mode = "a", header = not os.path.exists(path_global), index=False)
                pb_i.add(1)
            if save_epoch_weights:
                path_history_weights_model = os.path.join(path_all_history, "weights", str(str(epoch)+"_epoch"))
                os.makedirs(path_history_weights_model, exist_ok=True)              
                self.model.save_weights(os.path.join(path_history_weights_model, "checkpoint.h5"), save_format="h5")
            


    def predict(self, path_list, patients_list, path_history, load_weights=False, path_weight="", name_weight="checkpoint.h5", epoch=None, save_csv=False):
        if load_weights:
            if os.path.isfile(os.path.join(path_weight, name_weight)):
                logger.info(f"Loading weights from {path_weight}")
                self.model(np.zeros((1,*reversed(self.input_shape), self.n_channels)))
                self.model.load_weights(os.path.join(path_weight, name_weight))
            else:
                logger.info("Cannot find any model to load")
                return

        for patient_num in len(path_list):
            logger.info("PREDICTION")
            logger.info(f"I am at {path_list[patient_num]}.Initialize environment")
            if epoch is None and os.path.isfile(os.path.join(path_weight, "model_epoch.txt")):
                with open(os.path.join(path_weight,'model_epoch.txt'), "r") as f:
                    line = f.readline()
                    line = line.split(",")
                    epoch = int(line[0])
                    patient_num = int(line[1])
                    step = int(line[2])
            env = environment(patient = patients_list[patient_num],
                              path_to_FIFO= os.path.join(path_list[patient_num], "check"),
                              path= path_list[patient_num],
                              script = "refineMask", space ="AfterRefineLabel.ctx", 
                              dist_map = "DistanceTransform.ctx", new_shape=self.input_shape,
                              path_history = path_history,
                              epoch=epoch if  not None else f"{epoch}_{patient_num}_{step}",
                              start_TRiP=False,
                              save_csv=save_csv,
                              debug=False
                              )


            #tf.convert_to_tensor(np.expand_dims(state_t0, axis=0), dtype=tf.float32)
            inp = env.get_space()
            # state_t0 =np.expand_dims( env.get_state(), axis=0)
            action = self.model.predict(np.expand_dims(inp, axis=0))
            #TODO: pack into a fct
            prob=action.numpy()
            dist = tfp.distributions.Categorical(probs=prob, dtype=tf.float32)
            #sample an actioon depending on the distribution
            action = dist.sample()
            action = action.numpy()[0]

            mask = ctx()
            _map = mask.read(os.path.join(path_list[patient] , "AfterRefine.ctx"))
            # label_map = env.postprocessing(label_map)

            test_mask = copy.deepcopy(mask)
            test_mask.dimx = env.new_shape[0]
            test_mask.dimy = env.new_shape[1]
            test_mask.dimz = env.new_shape[2]


            action_to_write = np.where(env.label_new_shape_full>0,1,0)
            action_to_write = np.where(action==1,2,action_to_write)
            # action_to_write = np.where(action==1,2,0)
            test_mask.voi = action_to_write
            test_mask.write(path_history, str(patients_list[patient]+"_test"))

            action_to_write = np.where(env.label_new_shape_full==0,0,action_to_write)
            action_to_write = env.postprocessing(action_to_write)
            mask.voi = action_to_write
            mask.write(path_list[patient], "my_mask")


    def save(self, path = os.getcwd()):
        path_pretrained_model = os.path.join(path_all_history, "final_model")
        os.makedirs(path_pretrained_model, exist_ok=True)
        logger.info("Saving final model")
        self.model.save_weights(os.path.join(path_pretrained_model,'checkpoint.h5'), save_format="h5")