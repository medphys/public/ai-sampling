# -*- coding: utf-8 -*-
import numpy as np
import os


class ctx():
    '''read VOI from *.ctx and *.hed to '''
    def __init__(self):
        self.version = 1.2
        self.modality = "CT"
        self.primary_view = "transversal"
        self.data_type = None
        self.num_bytes = None
        self.byte_order = None
        self.patient_name = None
        self.slice_dimension = None
        self.pixel_size = None
        self.slice_distance = None
        self.slice_number  = None
        self.xoffset  = None
        self.dimx  = None
        self.yoffset  = None
        self.dimy  = None
        self.zoffset  = None
        self.dimz  = None
        self.var_write_ctx = ['version', 'modality', 'primary_view', 'data_type', 'num_bytes', 'byte_order', 'patient_name', 'slice_distance', 'slice_dimension', 'pixel_size', 'slice_number', 'xoffset', 'dimx', 'yoffset', 'dimy', 'zoffset', 'dimz']



    def read(self,path):
        '''read VOI from *.ctx and *.hed '''
        #splt the path to switch between .ctx and .hed
        f_split = os.path.splitext(path)
        header_file = f_split[0] + ".hed"
        #create a list of parameters from hed which should be always int
        int_list = ['num_bytes', 'slice_number', 'dimx', 'dimy', 'dimz']
        #try to open the *.hed file
        try:
            f = open(header_file,"r")
            #split into list of lists
            content = [x.split(' ') for x in f.read().split('\n')]
            #transforme into dict
            content_dict = dict((x[0], x[1]) for x in content if len(x)>=2)
            #sort out values which are define as class variables
            #newDict = {key: value for (key, value) in content_dict.items() if key in  list(self.__dict__.keys()) }

            #change the type of some values in dict into int or float
            newDict = {key : (int(val) if key in  int_list else val) for key, val in content_dict.items()}
            newDict = {key : (float(val) if key in ["pixel_size", 'slice_distance'] else val) for key, val in newDict.items()}
            #substitute class variables with found values
            #self.__dict__= newDict
            #substitute values in class var with values from .hed
            #TODO: what if var from hed not in class var
            self.__dict__ = {key: newDict.get(key, self.__dict__[key]) for key in self.__dict__}
        except OSError:
            print('Cannot open', path)
        else:
            f.close()


        # dt = ""
        dt =">" if self.byte_order == "aix" else "<"
        if self.data_type == "integer" and (self.num_bytes == 1 or self.num_bytes == 2 or self.num_bytes == 4 ): dt += "i"+str(self.num_bytes)
        elif self.data_type == "float" and (self.num_bytes == 4 or self.num_bytes == 8):dt += "f"+str(self.num_bytes)
        else: print("Unknown data type format")



        ctx_file = f_split[0] + ".ctx"
        # print(f"Trying to open {ctx_file} at {os.getcwd()}")
        cube = np.fromfile(ctx_file,dtype=dt)
        #cube = cube.byteswap()
        cube = cube.reshape(self.dimz, self.dimy,self.dimx)
        self.voi = cube
        return cube

    def write(self,path, mask_name = "my_mask", offset=False):
        path = os.path.join(path+"/"+ mask_name)
        # new_d = {key: value for (key, value) in self.__dict__.items() if key in  list(self.var_write_ctx) }
        # new_d = {str(key)+" "+ str(value)+"\n" for key, value in new_d.items()}
        # new_d=''.join(new_d)
        new_d="\n".join(("version "+self.version,
                "modality "+self.modality,
                "primary_view "+self.primary_view,
                "data_type "+self.data_type,
                "num_bytes "+str(self.num_bytes),
                "byte_order "+self.byte_order,
                "patient_name "+str(self.patient_name),
                "slice_dimension "+str(self.slice_dimension),
                "pixel_size "+str(self.pixel_size),
                "slice_distance "+ str(self.slice_distance),
                "slice_number "+str(self.slice_number),
                "xoffset "+str(self.xoffset if offset else 0),
                "dimx "+str(self.dimx),
                "yoffset "+str(self.yoffset if offset else 0),
                "dimy "+str(self.dimy),
                "zoffset "+ str(self.zoffset if offset else 0),
                "dimz "+str(self.dimz)))
        # new_d=new_d.format(self.version)
        # new_d=new_d.format(, , , , , ,  ,, ,,  ,, )

        try:
            with open(path+".hed", 'w') as f:

                f.write(new_d)
        except OSError:
            print('Cannot write', path)

        # dt = ""
        dt =">" if self.byte_order == "aix" else "<"
        if self.data_type == "integer" and (self.num_bytes == 1 or self.num_bytes == 2 or self.num_bytes == 4 ): dt += "i"+str(self.num_bytes)
        elif self.data_type == "float" and (self.num_bytes == 4 or self.num_bytes == 8):dt += "f"+str(self.num_bytes)
        else: print("Unknown data type format")


        try:
            with open(path+".ctx", 'wb') as f:

                np.array(self.voi, dtype=dt).tofile(f)

        except OSError:
            print('Cannot write', path)
