# -*- coding: utf-8 -*-
from tensorflow.keras import layers, Model

def ConvBlock(inp, dilation_rate, block_num, block_type="double", filters=64, strides=1):
    """ """
    
    
    # Convolutional layers
    conv1 = layers.Conv3D(  filters=filters,
                        kernel_size=(3,3,3),
                        strides=(strides,strides,strides),
                        padding='same',
                        dilation_rate=dilation_rate,
                        groups=1,
                        activation='relu',
                        use_bias=True,
                        kernel_initializer='glorot_uniform',
                        bias_initializer='zeros',
                        kernel_regularizer=None,
                        bias_regularizer=None,
                        activity_regularizer=None,
                        kernel_constraint=None,
                        bias_constraint=None,
                        name=f"Block_{block_num}_{block_type}"
                                        )
        
    conv2 = layers.Conv3D(  filters=filters,
                        kernel_size=(3,3,1),
                        strides=(strides,strides,strides),
                        padding='same',
                        dilation_rate=dilation_rate,
                        groups=1,
                        activation='relu',
                        use_bias=True,
                        kernel_initializer='glorot_uniform',
                        bias_initializer='zeros',
                        kernel_regularizer=None,
                        bias_regularizer=None,
                        activity_regularizer=None,
                        kernel_constraint=None,
                        bias_constraint=None
                                        )
    conv3 = layers.Conv3D(  filters=filters,
                        kernel_size=(3,3,1),
                        strides=(strides,strides,strides),
                        padding='same',
                        dilation_rate=dilation_rate,
                        groups=1,
                        activation='relu',
                        use_bias=True,
                        kernel_initializer='glorot_uniform',
                        bias_initializer='zeros',
                        kernel_regularizer=None,
                        bias_regularizer=None,
                        activity_regularizer=None,
                        kernel_constraint=None,
                        bias_constraint=None
                                        )
    
    conv4 = layers.Conv3D(  filters=filters/4,
                        kernel_size=(1,1,1),
                        strides=(strides,strides,strides),
                        padding='same',
                        dilation_rate=(1,1,1),
                        groups=1,
                        activation='relu',
                        use_bias=True,
                        kernel_initializer='glorot_uniform',
                        bias_initializer='zeros',
                        kernel_regularizer=None,
                        bias_regularizer=None,
                        activity_regularizer=None,
                        kernel_constraint=None,
                        bias_constraint=None,
                        name=f"Block_{block_num}_output"
                                        )
    # First convolutional block
    x = conv1(inp)
    
    # Second convolutional block
    x = conv2(x)
    
    if block_type=='triple':
        # Third convolutional block
        x = conv3(x)
    
    # Last convolutional block
    o = conv4(x)

    return x, o

def outputBlock(inp,dilation_rate=(1,1,1), filters=64,
            strides=1, n_out=2):
    """ """

    # Convolutional layers
    conv1 = layers.Conv3D(  filters=filters,
                        kernel_size=(1,1,1),
                        strides=(strides,strides,strides),
                        padding='same',
                        dilation_rate=dilation_rate,
                        groups=1,
                        activation='relu',
                        use_bias=True,
                        kernel_initializer='glorot_uniform',
                        bias_initializer='zeros',
                        kernel_regularizer=None,
                        bias_regularizer=None,
                        activity_regularizer=None,
                        kernel_constraint=None,
                        bias_constraint=None
                                        )
    
    conv2 = layers.Conv3D(  filters=n_out,
                        kernel_size=(3,3,3),
                        strides=(strides,strides,strides),
                        padding='same',
                        dilation_rate=dilation_rate,
                        groups=1,
                        activation='linear',
                        use_bias=True,
                        kernel_initializer='glorot_uniform',
                        bias_initializer='zeros',
                        kernel_regularizer=None,
                        bias_regularizer=None,
                        activity_regularizer=None,
                        kernel_constraint=None,
                        bias_constraint=None
                                        )
        
   
    
    s=layers.Softmax()

    # First convolutional block
    x = conv1(inp)
    
    # Second convolutional block
    x = conv2(x)
    
    # Softmax block
    x=s(x)

    return x

def architecture(input_shape,n_channels):

    inputs = layers.Input(shape=(*reversed(input_shape),n_channels))
     
    x, o1 = ConvBlock(inputs,(1,1,1),1)
    x, o2 = ConvBlock(x,(2,2,2),2)
    x, o3 = ConvBlock(x,(3,3,3),3,'triple')
    x, o4 = ConvBlock(x,(4,4,4),4,'triple')
    x, o5 = ConvBlock(x,(5,5,5),5,'triple')
    
    conc = layers.concatenate([o1, o2, o3, o4, o5])
    outputs=outputBlock(conc)
    
    
    model = Model(inputs=inputs, outputs=outputs)

    return model

