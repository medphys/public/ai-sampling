# -*- coding: utf-8 -*-

import logging
import sys
import os

def logger(stream = True, write_file = False, path =  os.getcwd(), name  = "log.log", level="info"):
    logger = logging.getLogger(__name__)
    if level.lower()=="debug":
        logger.setLevel(logging.DEBUG)
    elif level.lower()=="info":
        logger.setLevel(logging.INFO)
    else:
        print("Unknown loggger lever. The level is set to info.")
        logger.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt='[%(asctime)s] %(levelname)s %(funcName)s: %(message)s', datefmt='%d-%b-%y %H:%M:%S')
    handlers = []
    if stream:
        handlers.append(logging.StreamHandler(sys.stdout))
    if write_file:

        handlers.append(logging.FileHandler(os.path.join(path,name)))
    if handlers:
        for  h in handlers:
            h.setFormatter(formatter)
            logger.addHandler(h)
    return logger
