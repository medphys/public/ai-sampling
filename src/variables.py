#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  2 12:20:04 2024

@author: aquarz
"""
import os
from datetime import datetime
from log_conf import logger

#number of inputs
n_channels=3
#input shape for the model
input_shape = (114,104,40)
#name of the FIFO file
fifo_name = "fifo"
#path where all related data will be saved. Default is the working directory
path_all_history = os.getcwd()
path_to_data = os.getcwd()
os.makedirs(path_all_history, exist_ok=True)

new_folders = ["history", "weights", "log", "checkpoints"]
for f in new_folders:
    vars()[f"path_{f}"] = os.path.join(path_all_history, f)
    os.makedirs(vars()[f"path_{f}"], exist_ok=True)


log_path = os.path.join(path_all_history, "log")
time_var = datetime.now().strftime("%d.%m.%Y_%H.%M")
log_name = str("Log_"+time_var+".log")
logger=logger(stream = True, write_file = True, path =  log_path, name  = log_name, level="debug")


path = os.path.join(path_to_data, "Dataset/train")
dirlist = os.listdir(path)
pathlist = []

for folder in dirlist:
    subpath = os.path.join(path,folder)
    filelist = os.listdir(subpath)
    pathlist.append(subpath)
    #remove all existing FIFO files to avoid errors
    for file in filelist:
        if file==fifo_name:
            os.remove(os.path.join(subpath,file))

# pathlist.sort()
# dirlist.sort()


path_test = os.path.join(path_all_history, "Dataset/test")
dirlist_test = os.listdir(path_test)
pathlist_test = []
patient_list_test = []
for folder in dirlist_test:
    subpath = os.path.join(path_test,folder)
    filelist = os.listdir(subpath)
    pathlist_test.append(subpath)

    for file in filelist:
        if file==fifo_name:
            os.remove(os.path.join(subpath,file))
# pathlist = np.repeat(pathlist, 5)

pathlist_test.sort()
dirlist_test.sort()
