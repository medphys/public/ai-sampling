# -*- coding: utf-8 -*-
from ctx import ctx
import numpy as np
import pandas as pd
import errno
import os
import subprocess
from variables import logger, time_var

import shutil

class environment():

    def __init__(self, 
                 path_to_FIFO, 
                 path, 
                 script, 
                 space, 
                 random_mask = None, 
                 label_map = None,
                 epoch=0,
                 target_label = 2, 
                 dose_map = None, 
                 dist_map = None,  
                 new_shape = None,
                 labels= "RM", 
                 patient = "", 
                 path_history = os.getcwd(), 
                 start_TRiP=False, 
                 path_TRiP="TRiP98",
                 # resume_training=False,
                 save_csv=False,
                 debug=False):
        """Creates environment to enable TRiP-AI communcation
        Parameters
        ----------
        path_to_FIFO : str
            path to FIFO file
        path : str
            path to the patient folder
        script : str
            name of the exec file
        space : str
            name of the file with labeled VOIs used to create space
        random_mask : str
            name of the file with selected voxels from the original script  (default is None)
        label_map : str
            name of the file with selected voxels and its VOI specific labels (default is None)
        epoch : int
            number of the current epoch. Used for log and CSV file
        target_label : int
            label of the VOI from space used for target
        dose_map : str
            name of the file with dose values from the read mask (default is None)
        dist_map : str
            name of the file with distance transformation in space   (default is None)
        new_shape : tuple
            new shape of the input if preprocessing is needed
        labels : str
            labels for OARs. RM for the voxels from the initial script or FM for all voxels  (default is RM)   
        patient : str
            name of the patient used for log and CSV file
        path_history : str
            path to write the history  (default is working directory)  
        start_TRiP : bool
            need to start TRiP98 (default is False)
        path_TRiP : str
             path to TRiP98    
        save_csv : bool
            saves CSV file with dose values from TRiP98. If False the values will not be read.
        debug : bool
            saves dose map, label map and mask for TRiP98 to the history
        """
        #HISTORY
        self.path_history = path_history
        self.patient = patient
        self.path = path
        self.path_to_FIFO = path_to_FIFO
        self.count_steps = 0
        self.epoch=epoch
        self.labels=labels
        self.path_TRiP=path_TRiP
        self.save_csv=save_csv
        self.create_environment(start_TRiP=start_TRiP,
                                path=path, 
                                script=script, 
                                space=space, 
                                label_map=label_map,
                                random_mask=random_mask,
                                new_shape=new_shape, 
                                target_label=target_label, 
                                labels=labels,
                                patient=patient, 
                                dose_map=dose_map, 
                                dist_map=dist_map, 
                                epoch=self.epoch)

        self.debug=debug
    def create_environment(self,start_TRiP, path, script, space, label_map,
                           random_mask, new_shape, target_label, labels,
                           patient, dose_map=None, dist_map=None, epoch=0):
        
        #start TRiP98 or check whether all needed files are there to create 
        #the environment without starting TRiP98
        if start_TRiP or\
            not os.path.isfile(os.path.join(path,space)) or\
            random_mask is not None and not os.path.isfile(os.path.join(path,random_mask)) or\
            label_map is not None and not os.path.isfile(os.path.join(path,label_map)) or\
            dose_map is not None and not os.path.isfile(os.path.join(path,dose_map)) or\
            dist_map is not None and not os.path.isfile(os.path.join(path,dist_map)):
            self.history_DB = pd.DataFrame({'Epoch':[],
                                            'Sample':[],
                                             'PatientID' : [],
                                             'Category' : [],
                                             'Parameter' : [],
                                             'criteria' : [],
                                             'label' : [],
                                             'FM' : [],
                                             'FM_counter' : [],
                                             'RM' : [],
                                             'RM_counter' : [],
                                             'RM_counter_%' : [],
                                             })
            self.start_trip(path, script, self.path_TRiP)
            #does first read in FIFO
            self.FIFO_first_run(self.path_to_FIFO)
            if start_TRiP==False:
                self.stop_env()
        
        #read all data
        #create a mask used as a template for writing .ctx
        self.mask = ctx()
        self.space = self.mask.read(os.path.join(path,space))
        self.unique_labels = np.unique(self.space)
        self.unique_labels = np.delete(self.unique_labels, np.where((self.unique_labels == 0)|(self.unique_labels == 1)))
        self.target_label = target_label
        self.target_cube = np.where(self.space == self.target_label, 1, 0)
        self.rest_voi_cube =  np.where(self.space > self.target_label, 1, 0)
        self.dim_shape = self.space.shape
        self.new_shape = new_shape
        
        self.random_mask = ctx().read(os.path.join(path,random_mask))
        self.label_map_name = label_map
        self.label_map = ctx().read(os.path.join(path,label_map))
        self.dose_map_name = dose_map
        
        if dose_map is not None and os.path.isfile(os.path.join(path,dose_map)):
            self.dose_map = ctx().read(path + "/" + dose_map) if dose_map is not None else None
        if dist_map is not None and os.path.isfile(os.path.join(path,dist_map)):
            self.dist_map = ctx().read(path + "/" + dist_map) if dist_map is not None else None

        logger.debug("Get space")
        self.create_space([self.dist_map, self.target_cube, self.rest_voi_cube])
        self.label_new_shape = self.preprocessing(self.label_map, self.new_shape)
        self.label_new_shape_full = self.preprocessing(self.space, self.new_shape)
        self.random_mask_new_shape = self.preprocessing(self.random_mask, self.new_shape)
        
        if not os.path.isfile(os.path.join(path, "database.csv")) and self.save_csv:
            for c in ["FM_counter", "RM_counter"]:
                counter = self.create_label_counter(self.space if c.startswith("FM") else self.label_map, c) 
                key_list = list(self.history_DB['label'])
                dict_lookup = dict(zip(counter['label'], counter[c]))
                self.history_DB[c] = [dict_lookup[item] for item in key_list]

            self.history_DB["RM_counter_%"] = self.history_DB["RM_counter"]*100/self.history_DB["FM_counter"]
            self.FIFO_get_FM(self.path_to_FIFO)

            logger.debug(f"TRiP values\n{self.history_DB}")

            self.history_DB = self.history_DB.append({"Category":"Voxel", "Parameter":"Num",
                                                                "FM_counter": self.history_DB["FM_counter"].sum(),
                                                                "RM_counter": self.history_DB["RM_counter"].sum(),
                                                                "RM_counter_%":self.history_DB["RM_counter"].sum()/self.history_DB["FM_counter"].sum()*100},
                                                               ignore_index=True)
            self.history_DB = self.history_DB.append({"Category":"Voxel", "Parameter":"out_voi", "FM_counter":np.prod(self.new_shape)-self.history_DB.loc[len(self.history_DB)-1,"FM_counter"]}, ignore_index=True)
            self.history_DB.loc[:,"PatientID"]=patient
            self.history_DB.to_csv(os.path.join(path, "database.csv"), index=False)
        elif self.save_csv:
            self.history_DB = pd.read_csv(os.path.join(path, "database.csv"))


    def write_history(self,path = None, Local = True, local_name = None, Global = True):
        if path is None:
            path = self.path_history
        if Global:
            path_global = os.path.join(path, "history_whole.csv")
            self.history_DB.to_csv(path_global, mode = "a", header = not os.path.exists(path_global), index=False)

        if Local:
            if local_name is None:
                local_name = str(self.patient+"_history_epoch_"+str(self.epoch)+".csv")
            path_local = os.path.join(path, self.patient)
            os.makedirs(path_local, exist_ok=True)
            self.history_DB.to_csv(os.path.join(path_local, local_name), index=False)
    def resume_history(self, path = None, local_name = None):
        if not self.save_csv: return
        if path is None:
            path = self.path_history
        if local_name is None:
            local_name = str(self.patient+"_history_epoch_"+str(self.epoch)+".csv")
        path_local = os.path.join(path, self.patient)
        if os.path.exists(os.path.join(path_local, local_name)):
            self.history_DB = pd.read_csv(os.path.join(path_local, local_name))
    def create_label_counter(self, mask, col_name = "num"):
        """ Create a data frame with unique labels and its number of voxels using user column name"""
        label_counter = pd.DataFrame({'label' : self.unique_labels.astype(int),
                                      col_name : self.count_AV(mask)
                                         })
        return label_counter

    def get_output(self):
        if self.labels=="RM":
            return self.preprocessing(self.random_mask, self.new_shape)
        elif self.labels=="FM":
            return np.where(self.label_new_shape_full>0,2,0)
    def get_labels(self):
        return self.label_new_shape_full
    def start_trip(self,  path, script, path_to_trip, wait = False):
        cmd=[f"""cd  {path}; {path_to_trip} -s -c "exec '{script}.exec' " > {script}_{time_var}_epoch_{self.epoch}.log 2>&1 """]
        logger.debug("Start TRiP")
        logger.debug(path)
        trip = subprocess.Popen(cmd, shell=True)
        if wait:
            trip.wait()
        else:
            trip.poll()

    def stop_env(self):
        logger.debug("Stop environment")
        with open(self.path_to_FIFO,"w") as fifo:
            logger.debug("FIFO opened to stop")
            fifo.write("STOP\0")


    def create_space(self, list_of_spaces): 
        """ create input for the model """
        list_of_spaces = [x for x in list_of_spaces if x is not None]
        self.whole_space = []
        
        for s in list_of_spaces:
            if self.new_shape!=None:
                s = self.preprocessing(s, self.new_shape)
            sp = np.expand_dims(s, axis=3)
            if len(self.whole_space)==0:
                self.whole_space = sp.copy()
            else:
                self.whole_space = np.concatenate((self.whole_space, sp), axis=3)



    def get_space(self):
        return self.whole_space
    
    #TODO add vaiable to read any and check existance
    def get_dose(self):#, dose_map_name=self.dose_map_name):
        return self.preprocessing(self.dose_map, self.new_shape)

    def print_values_counts(self, mask, mask_name="", output = False):
        values, counts = np.unique(mask, return_counts=True)
        if mask_name!="":
            logger.debug(f"Unique values in {mask_name} {values}")
            logger.debug(f"Number of unique values in {mask_name} {counts}")
        if output:
            return values, counts


    def step(self, action):
   
        self.evaluation(action)
        self.dose_map = ctx().read(self.path + "/" + self.dose_map_name) if self.dose_map is not None else None

        logger.debug(f"Evaluation values\n{self.history_DB}")

        if self.debug:
            #TODO add epoch folder
            path_history_dose_map = os.path.join(self.path_history, "dose_map")
            os.makedirs(path_history_dose_map, exist_ok=True)
            shutil.copy(os.path.join(self.path, self.dose_map_name), os.path.join(path_history_dose_map,str(self.patient+"_"+self.dose_map_name.split(".")[0]+"_epoch"+str(self.epoch)+"_"+str(self.count_steps)+".ctx")))
            shutil.copy(os.path.join(self.path, str(self.dose_map_name.split(".")[0]+".hed")), os.path.join(path_history_dose_map,str(self.patient+"_"+self.dose_map_name.split(".")[0]+"_epoch"+str(self.epoch)+"_"+str(self.count_steps)+".hed")))
            path_history_label_map = os.path.join(self.path_history, "label_map")
            os.makedirs(path_history_label_map, exist_ok=True)
            shutil.copy(os.path.join(self.path, self.label_map_name), os.path.join(path_history_label_map,str(self.patient+"_"+self.label_map_name.split(".")[0]+"_epoch"+str(self.epoch)+"_"+str(self.count_steps)+".ctx")))
            shutil.copy(os.path.join(self.path, str(self.label_map_name.split(".")[0]+".hed")), os.path.join(path_history_label_map,str(self.patient+"_"+self.label_map_name.split(".")[0]+"_epoch"+str(self.epoch)+"_"+str(self.count_steps)+".hed")))
    
            path_history_TRiP_mask = os.path.join(self.path_history, "TRiP_mask")
            os.makedirs(path_history_TRiP_mask, exist_ok=True)
            shutil.copy(os.path.join(self.path, "my_mask.ctx"), os.path.join(path_history_TRiP_mask,str(self.patient+"_"+"TRiP_mask_epoch"+str(self.epoch)+"_"+str(self.count_steps)+".ctx")))
            shutil.copy(os.path.join(self.path, "my_mask.hed"), os.path.join(path_history_TRiP_mask,str(self.patient+"_"+"TRiP_mask_epoch"+str(self.epoch)+"_"+str(self.count_steps)+".hed")))

        self.count_steps+=1



    def evaluation(self, action):
        mask_to_write = np.where(action==1, 2, 0)
        mask_to_write = np.squeeze(mask_to_write)
        mask_to_write = np.where((self.label_new_shape_full>0)&(mask_to_write==0), 1, mask_to_write)

        logger.debug(f"Shape of action before postprocessing {mask_to_write.shape}")
        mask_to_write = self.postprocessing(mask_to_write)

        mask_to_write = np.where(self.space == 0, 0, mask_to_write)
        self.mask.voi = mask_to_write
        logger.debug("Writing mask...")
        self.mask.write(self.path)

        with open(self.path_to_FIFO,"w") as fifo:
            logger.debug("FIFO opened to write")
            fifo.write("my_mask.ctx\0")
        logger.debug("Finished writing")
        logger.debug("Writing mask to the history folder")
        logger.debug("finished writing the history")
        variable_list = []
        with open(self.path_to_FIFO) as fifo:
            logger.debug("FIFO opened")
            while True:
                data = fifo.read()
                if  self.save_csv:
                    data = data.strip("\0")
                    data = data.split()
                    for element in data:
                        variable_list.append(element.strip("\0"))

                if len(data) == 0:
                    logger.debug("Writer closed")
                    break
       
        label_map = ctx().read(self.path + "/" + self.label_map_name)
        self.label_map_new = label_map
        self.label_new_shape_new = self.preprocessing(self.label_map_new, self.new_shape)
        if self.save_csv:
            variable_list = np.reshape(variable_list, (int(len(variable_list)/6),6))
            target_vars = pd.DataFrame(variable_list, columns = ["Category", "Parameter",  "nn", "label", "criteria", "value"])
            logger.debug(f"Values after evaluation \n{target_vars}")
            
            out_of_voi = np.zeros(self.label_new_shape_full.shape)
            out_of_voi = np.where((self.label_new_shape_full==0) & (action==1), 1, out_of_voi)
            num_AV_out_voi = np.count_nonzero(out_of_voi == 1)
            logger.debug(f"Out of VOI: {num_AV_out_voi}")
            logger.debug(f"Out of VOI %: {num_AV_out_voi/(np.prod(self.new_shape)-self.history_DB.loc[self.history_DB.Parameter=='Num','FM_counter'].values[0])*100}")
            
            logger.debug("Writing the history values")
            counter = self.create_label_counter(label_map, "NM_counter")
            
            self.history_DB.loc[:,f"NM_{self.count_steps}"] = pd.Series([*target_vars["value"].astype(float), 0, 0])
            key_list = list(self.history_DB['label'].dropna())
            dict_lookup = dict(zip(counter['label'], counter["NM_counter"]))

            
            temp=[dict_lookup[item] for item in key_list]
            self.history_DB.loc[:,f"NM_counter_{self.count_steps}"] = pd.Series([*temp, sum(temp), num_AV_out_voi])
            self.history_DB.loc[:,f"NM_counter_%_{self.count_steps}"] = pd.Series([*(self.history_DB.loc[self.history_DB.Category=="VOI", f"NM_counter_{self.count_steps}"]*100/self.history_DB.loc[self.history_DB.Category=="VOI", "FM_counter"]).tolist(), 
                                                                                   (self.history_DB.loc[self.history_DB.Parameter=="Num",f"NM_counter_{self.count_steps}"]/self.history_DB.loc[self.history_DB.Parameter=="Num","FM_counter"]*100).values[0],
                                                                               num_AV_out_voi*100/self.history_DB.loc[len(self.history_DB)-2, "FM_counter"]])
        

    def FIFO_first_run(self, path_to_FIFO):
        try:
            os.mkfifo(path_to_FIFO)
        except OSError as oe:
            if oe.errno != errno.EEXIST:
                raise
        logger.debug("Opening FIFO...")
        variable_list = []
        with open(path_to_FIFO) as fifo:
            logger.debug("FIFO opened")
            while True:
                data = fifo.read()
                if  self.save_csv:
                    data = data.strip("\0")
                    data = data.split()
                    for element in data:
                        variable_list.append(element.strip("\0"))

                if len(data) == 0:
                    logger.debug("Writer closed")
                    break
        if  self.save_csv:
            variable_list = np.reshape(variable_list, (int(len(variable_list)/6),6))
            target_vars = pd.DataFrame(variable_list, columns = ["Category", "Parameter",  "nn", "label", "criteria", "value"])
            self.history_DB["Category"]=target_vars["Category"]
            self.history_DB["Parameter"]=target_vars["Parameter"]
            self.history_DB["criteria"]=target_vars["criteria"]
            self.history_DB["label"]=target_vars["label"].astype(int)
            self.history_DB["RM"]=target_vars["value"].astype(float)
            



    def FIFO_get_FM(self, path_to_FIFO):
        if not self.save_csv:return
        mask_to_write = np.where(self.space>0,2,0)
        self.mask.voi = mask_to_write
        logger.debug("Writing mask...")
        self.mask.write(self.path)
        with open(self.path_to_FIFO,"w") as fifo:
            logger.debug("FIFO opened to write")
            fifo.write("my_mask.ctx\0")
        logger.debug("Opening FIFO...")
        variable_list = []
        with open(path_to_FIFO) as fifo:
            logger.debug("FIFO opened")

            while True:
                data = fifo.read()
                data = data.strip("\0")
                data = data.split()
                for element in data:
                    variable_list.append(element.strip("\0"))

                if len(data) == 0:
                    logger.debug("Writer closed")
                    break

        variable_list = np.reshape(variable_list, (int(len(variable_list)/6),6))
        target_vars = pd.DataFrame(variable_list, columns = ["ind", "Parameter",  "nn", "label", "criteria", "value"])
        self.history_DB["FM"]=target_vars["value"].astype(float)



    def count_AV(self, mask):
        counted_AV = []
        for label in self.unique_labels:
            num_AV = len(np.asarray(np.where(mask == label)).T)
            counted_AV.append(num_AV)
        return counted_AV

    def preprocessing(self, voi, new_shape):
        """
        Add zero levels to the axis: 0 for z, 1 for y, 2 for x
        
        """

        self.dimx=voi.shape[2]
        self.dimy=voi.shape[1]
        self.dimz=voi.shape[0]

        dimx = new_shape[0]
        dimy = new_shape[1]
        dimz = new_shape[2]



        #TODO: function checkinf dim var are posivie int
        #TODO dimx_prep remove self
        if dimx>self.dimx:
            self.dimx_prep=voi.shape[2]
            self.dimy_prep=voi.shape[1]
            self.dimz_prep=voi.shape[0]
            
            dif=(dimx-self.dimx)
            self.addx_0=int(dif/2)
            self.addx_1=int(dif-self.addx_0)
            voi = np.concatenate((np.zeros((self.dimz_prep,self.dimy_prep,self.addx_0)),voi), axis=2)
            voi = np.concatenate((voi, np.zeros((self.dimz_prep,self.dimy_prep,self.addx_1))), axis=2)

        if dimy>self.dimy:
            self.dimx_prep=voi.shape[2]
            self.dimy_prep=voi.shape[1]
            self.dimz_prep=voi.shape[0]
            dif=(dimy-self.dimy)
            self.addy_0=int(dif/2)
            self.addy_1=int(dif-self.addy_0)
            voi = np.concatenate((np.zeros((self.dimz_prep,self.addy_0,self.dimx_prep)),voi), axis=1)
            voi = np.concatenate((voi, np.zeros((self.dimz_prep,self.addy_1,self.dimx_prep))), axis=1)

        if dimz>self.dimz:
            self.dimx_prep=voi.shape[2]
            self.dimy_prep=voi.shape[1]
            self.dimz_prep=voi.shape[0]
            dif=(dimz-self.dimz)
            self.addz_0=int(dif/2)
            self.addz_1=int(dif-self.addz_0)
            voi = np.concatenate((np.zeros((self.addz_0, self.dimy_prep,self.dimx_prep)),voi), axis=0)
            voi = np.concatenate((voi, np.zeros((self.addz_1, self.dimy_prep,self.dimx_prep))), axis=0)

        self.dimx_prep=voi.shape[2]
        self.dimy_prep=voi.shape[1]
        self.dimz_prep=voi.shape[0]
        logger.debug(f"New shape of label map: {voi.shape}")
        return voi


   


    def postprocessing(self, voi):
        """
        process to the original shape
        
        """
        voi_pos = voi[self.addz_0:voi.shape[0]-self.addz_1, self.addy_0:voi.shape[1]-self.addy_1,self.addx_0:voi.shape[2]-self.addx_1]
        logger.debug(f"Returned shape: {voi_pos.shape}")
        return voi_pos
