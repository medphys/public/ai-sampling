#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('src')
from model import AI_Sampling

from variables import pathlist
from variables import dirlist
from variables import path_checkpoints
from variables import path_all_history

import os

# print(path_all_history)
# print(pathlist[0])

#TODO
#add debug with save weights, save tables, log
agent = AI_Sampling(save_loss=False, 
                    save_softmax=False
                    )
agent.train(pathlist, #full path to each patient's data
            patients_list = dirlist, #default is each folder is the patient's name
            resume_training = False,
            path_resume=os.path.join(path_checkpoints,"checkpoint.h5"),
            path_save_checkpoint= path_checkpoints,
            epochs = 2,
            steps_pro_episode=2, 
            save_csv=True,
            path_TRiP="TRiP98")

agent.save(path_all_history)

